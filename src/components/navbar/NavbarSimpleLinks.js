import {
  Flex,
  HStack,
  Link,
  IconButton,
  Button,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  useDisclosure,
  Divider,
  Icon,
  useColorModeValue,
  useColorMode,
} from '@chakra-ui/react';
import { HamburgerIcon, CloseIcon } from '@chakra-ui/icons';
import PropTypes from "prop-types";

// Assets
import { IoMdMoon, IoMdSunny } from "react-icons/io";

const Links = [{ name: 'PreSale', url: "#/presale" },
{ name: 'NFTMarket', url: "#/nftmarket" },
{ name: 'Game', url: "#" },
{ name: 'Vesting', url: "#" },
{ name: 'Staking', url: "#" },
{ name: 'P2P', url: "#" },
{ name: 'Maitrix', url: "#/" }];

const Languages = ['English', 'Spanish'];

const NavLink = ({ item }) => (
  <Link
    padding={2}
    px={6}
    rounded={'md'}
    _hover={{
      textDecoration: 'none',
      bg: useColorModeValue('gray.200', '#e51c50'),
    }}
    href={item.url}>
    {item.name}
  </Link>
);

export default function NavbarSimpleLinks(props) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { brandText, currentLanguage } = props;
  const { colorMode, toggleColorMode } = useColorMode();
  const navbarIcon = useColorModeValue("gray.400", "white");

  return (
    <>
      <Flex
        w="auto"
        alignItems='center'
        flexDirection='row'
        p='20px'
        justifyContent={'space-between'}>
        <IconButton
          size={'md'}
          icon={isOpen ? <CloseIcon /> : <HamburgerIcon />}
          aria-label={'Open Menu'}
          display={{ md: 'none' }}
          onClick={isOpen ? onClose : onOpen}
        />
        <HStack
          spacing={16}
          alignItems={'center'}>
          <Link
            fontSize={32}
            paddingLeft="30%"
            paddingRight={10}
            href='/'>
            {brandText}
          </Link>
          <Divider />
          <HStack
            as={'nav'}
            display={{ base: 'none', md: 'flex' }}
          >
            {Links.map((item, key) => (
              <NavLink key={key.toString()} item={item} />
            ))}
          </HStack>
        </HStack>
        <Flex alignItems={'center'}>
          <Button
            variant='no-hover'
            bg='transparent'
            p='0px'
            minW='unset'
            minH='unset'
            h='18px'
            w='max-content'
            onClick={toggleColorMode}>
            <Icon
              me='10px'
              h='18px'
              w='18px'
              color={navbarIcon}
              as={colorMode === "light" ? IoMdMoon : IoMdSunny}
            />
            </Button>
            <Menu>
              <MenuButton
                as={Button}
                rounded={'full'}
                variant={'link'}
                cursor={'pointer'}
                minW={0}>
                {currentLanguage}
              </MenuButton>
              <MenuList>
                {Languages.map((language, index) => (
                  <MenuItem key={index.toString()}>{language}</MenuItem>
                ))}
              </MenuList>
            </Menu>
        </Flex>
      </Flex>
    </>
  );
}

NavbarSimpleLinks.propTypes = {
  brandText: PropTypes.string,
  currentLanguage: PropTypes.string
};
