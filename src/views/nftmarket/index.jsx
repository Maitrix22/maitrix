// Chakra imports
import {
  Box,
  Grid,
  SimpleGrid,
  useColorModeValue,
  Center,
  useToast,
  Alert,
  AlertIcon,
  AlertTitle,
  AlertDescription,
  Text,
  Flex
} from "@chakra-ui/react";

import Deposit from "./components/Deposit";
import { useCountdown } from "hooks/useCountdown";
import timerBg from "assets/img/backgrounds/timer_bg.png";

import React, { useState, useEffect } from "react";
import { useWeb3React } from "@web3-react/core";
import { contractAddress, tokenPresaleContract, busdContract, convertToWei, convertFromWei } from "interact";
import { connectors } from "connectors";
import NFT from "views/home/components/NFT";
//assets
import Nft1 from "assets/img/nfts/nft1.jpg";

export default function Overview() {

  const brandColor = useColorModeValue("brand.500", "white");
  const boxBg = useColorModeValue("secondaryGray.300", "whiteAlpha.100");

  const {
    //library,
    chainId,
    account,
    activate,
    deactivate,
    active
  } = useWeb3React();

  const [signature, setSignature] = useState("");
  const [error, setError] = useState("");
  const [provider, setProvider] = useState(undefined);
  const [message, setMessage] = useState("");
  const [signedMessage, setSignedMessage] = useState("");
  const [verified, setVerified] = useState();
  const [maxToken, setMaxToken] = useState(0);
  const [usedToken, setUsedToken] = useState(0);
  const [amount, setAmount] = useState(0);
  const [isApprove, setIsApprove] = useState(false);
  const [balance, setBalance] = useState(0);
  const [busdBalance, setBusdBalance] = useState(0);
  const [claimedAmount, setClaimedAmount] = useState(0);
  const [state, setState] = useState();
  const [canClaim, setCanClaim] = useState(false);
  const [currentClaim, setCurrentClaim] = useState(0);
  const [days, hours, minutes, seconds] = useCountdown(new Date(2022, 12, 31));

  const toast = useToast();

  const refreshState = () => {
    window.localStorage.setItem("provider", undefined);
    setMessage("");
    setProvider("");
    setSignature("");
    setBalance(0);
    setCanClaim(false);
    setVerified(undefined);
  };

  const disconnect = () => {
    refreshState();
    deactivate();
  };

  const getMaxToken = async () => {
    const total = await tokenPresaleContract.methods.MAX_TOTAL().call();
    setMaxToken(convertFromWei(total));
  }

  const getUsedToken = async () => {
    const total = await tokenPresaleContract.methods.totalDeposited().call();
    setUsedToken(convertFromWei(total));
  }

  const getCanClaim = async () => {
    const canClaim = await tokenPresaleContract.methods.isClaimActive().call();
    setCanClaim(canClaim);
  }

  const getBalance = async () => {
    if (account === undefined)
      return;
    const total = await tokenPresaleContract.methods.deposits(account).call();
    setBalance(convertFromWei(total));
  }

  const deposit = async (amount) => {
    await tokenPresaleContract.methods.deposit(amount).send({ from: account });
  }

  const claim = async () => {
    await tokenPresaleContract.methods.claim().send({ from: account });
  }

  const calculateClaim = async () => {

    var total = '0';
    if (!canClaim && !account)
      return;

    try {
      console.log("entro aqui " + account);
      total = await tokenPresaleContract.methods.calculcateClaim(account).call();
    } catch (err) {
      console.log(err);
    }
    setCurrentClaim(convertFromWei(total));
  }

  const approve = async (spender, amount) => {
    await busdContract.methods.approve(spender, amount).send({ from: account });
  }

  const getBusdBalance = async () => {
    if (!account)
      return;
    const balance = await busdContract.methods.balanceOf(account).call();
    console.log("balance: " + balance);
    setBusdBalance(convertFromWei(balance));
  }

  const handleApproveClick = async () => {
    setState({ message: "Approving", status: "warning" });
    await approve(contractAddress, convertToWei('6000'));
    setIsApprove(true);
    setState({ message: "Approved", status: "success" });
  }

  const handleDepositClick = async () => {
    await deposit(convertToWei(amount.toString()));
    await getBalance();
    setState({ message: "Confirmed", status: "success" });
  }

  const handleClaimClick = async () => {
    await claim();
    await getBalance();
    await calculateClaim();
    setState({ message: "Claimed", status: "success" });
  }


  const handleDepositChange = (value) => {
    //const value = Number(event.target.value);
    setAmount(value);
  }

  useEffect(async () => {
    await getMaxToken();
  }, [maxToken]);

  useEffect(async () => {
    await getUsedToken();
  }, [usedToken]);

  useEffect(async () => {
    await getCanClaim();
  }, [canClaim]);

  useEffect(() => {
    const provider = window.localStorage.getItem("provider");

    console.log("provider " + provider);
    switch (provider) {
      case 'injected':
        activate(connectors.injected);
        setProvider(provider);
        break;
      case 'coinbaseWallet':
        activate(connectors.coinbaseWallet);
        setProvider(provider);
        break;
      case 'walletConnect':
        activate(connectors.walletConnect);
        setProvider(provider);
        break;
    }
  }, [provider]);

  useEffect(async () => {
    await getBalance();
    await getBusdBalance();
    await getCanClaim();
    await calculateClaim();
  }, [account]);

  useEffect(() => {
    if (state) {
      const { message, status } = state;

      toast({
        title: status,
        description: message,
        status: status,
        duration: 3000,
        position: "top",
        isClosable: true,
      });
    }
  }, [state]);

  return (
    <>
      <Box
        height={500}
        bgImage={timerBg}
        bgSize="cover"
        bgPosition="center"
        bgRepeat="no-repeat"
      >
        <Text
          align="center"
          fontWeight={900}
          py={250}
          fontSize={60}>
          {`${days}d ${hours}h ${minutes}m ${seconds}s`}
        </Text>
      </Box>
      <Center>
        <Box pt={{ base: "130px", md: "80px", xl: "80px" }} maxW='3xl' >
          {/* Main Fields */}
          {(chainId != 4 && active) ? (
            <Alert status='error' mb={2}>
              <AlertIcon />
              <AlertTitle>You are in the wrong network!</AlertTitle>
              <AlertDescription>Please switch to Binance Smart Chain.</AlertDescription>
            </Alert>) : null}
          {!active ? (
            <Alert status='error' mb={2}>
              <AlertIcon />
              <AlertTitle>Please Connect to your wallet!</AlertTitle>
              <AlertDescription></AlertDescription>
            </Alert>) : null}
        </Box>
      </Center>
      <Flex
        direction="column"
        align="left"
        py={50}
        maxW={{ xl: "1000px" }}
        m="0 auto">
        <SimpleGrid
          spacing={10}
          columns={{ base: 1, md: 2, lg: 2, xl: 2 }}
          gap='20px'
          m='20px'
          maxWidth="1200px"
        >
          <Box>
            <NFT
              maxW="300px"
              name='Abstract Colors'
              description='By Esthera Jackson'
              image={Nft1}
              download={'#'}
              currentbid='600 BUDS'
              showbids={true}
              showbidsbutton={false}
            />
          </Box>
          <Deposit
            isApproved={isApprove}
            title=''
            subtitle={`Deposit, wait and profit tokens, chainId`}
            active={active}
            canClaim={canClaim}
            disconnect={disconnect}
            handleDepositClick={handleDepositClick}
            handleApproveClick={handleApproveClick}
            handleClaimClick={handleClaimClick}
            getBalance={getBalance}
            balance={balance}
            amount={amount}
            busdBalance={busdBalance}
            handleDepositChange={handleDepositChange}
            setProvider={setProvider} />
        </SimpleGrid>
      </Flex>
    </>
  );
}
