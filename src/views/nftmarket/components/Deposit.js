// Chakra imports
import {
  Flex,
  Stat,
  useColorModeValue,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  Input,
  Button,
  Text,
  Stack,
  Badge,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
  Grid,
  SimpleGrid
} from "@chakra-ui/react";

import { CheckIcon } from "@chakra-ui/icons"
// Custom components
import Card from "components/card/Card.js";
import SelectWalletModal from "./SelectWalletModal.js"
// Custom icons
import React from "react";
import { useDisclosure } from '@chakra-ui/react'

export default function Default(props) {
  const { title, subtitle, isApproved, active, disconnect, amount, handleDepositClick, handleApproveClick, handleClaimClick, getBalance, busdBalance, handleDepositChange, canClaim, setProvider, balance } = props;
  const textColor = useColorModeValue("secondaryGray.900", "white");
  const textColorWhite = useColorModeValue("white", "white");
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <Card mb={{ base: "0px", lg: "20px" }} align='center'>
      <Flex
        align={{ sm: "flex-start", lg: "center" }}
        justify='space-between'
        w='100%'
        px='22px'
        pb='20px'
        mb='10px'
        boxShadow='0px 40px 58px -20px rgba(112, 144, 176, 0.26)'>
        {active && canClaim ? (
          <Button colorScheme='teal' variant='solid' onClick={handleClaimClick}>Claim</Button>
        ) : null}
        <Text color={textColor} fontSize='xl' fontWeight='600'>
          {title}
        </Text>
        <SelectWalletModal isOpen={isOpen} closeModal={onClose} setProvider={setProvider} />
          {!active ? (
            <Button onClick={onOpen} rounded={0} colorScheme='teal' fontSize={18} py={6}>Connect Wallet</Button>
          ) : (
            <Button onClick={disconnect} rounded={0} colorScheme='teal' fontSize={18} py={6}>Disconnect</Button>
          )}
      </Flex>
      <SimpleGrid
          fontSize={18}
          columns={{ base: 1, md: 1, lg: 2 }}
          gap='20px'
          mb='20px'
          width="auto"
        >
        <Text align="start" pl={5}>For Sale</Text>
        <Text borderWidth={1} padding={1}>1000 NFT</Text>
        <Text align="start"  pl={5}>Available</Text>
        <Text borderWidth={1} padding={1}>500 NFT</Text>
        <Text align="start"  pl={5}>Balance</Text>
        <Text borderWidth={1} padding={1}>{balance} NFT</Text>
        <Text align="start"  pl={5}>Amount</Text>
        <NumberInput colorScheme='brandScheme' value={amount} onChange={handleDepositChange}>
            <NumberInputField focusBorderColor='red.200' color={textColor} />
            <NumberInputStepper>
              <NumberIncrementStepper />
              <NumberDecrementStepper />
            </NumberInputStepper>
        </NumberInput>
          <Text align="start" pl={5}>Value</Text>
          <Text borderWidth={1} padding={1}>{amount*100} BUSD</Text>
      </SimpleGrid>
          {!active ? (
            <Button onClick={onOpen} rounded={0} colorScheme='teal' fontSize={18} py={6}>Connect Wallet</Button>
          ) : !isApproved ? (
            <Button rounded={0} colorScheme='teal' onClick={handleApproveClick} fontSize={18} py={6}>
              BUY
            </Button>
          ) : <Button rounded={0}  colorScheme='teal' onClick={handleDepositClick} fontSize={18} py={6}>
              DEPOSIT
          </Button>}
    </Card>
  );
}
