// Chakra imports
import {
  Box,
  Grid,
  SimpleGrid,
  Icon,
  useColorModeValue,
  Center,
  useToast,
  Alert,
  AlertIcon,
  AlertTitle,
  AlertDescription,
  Text,
  Stack,
  Divider,
  Flex,
  Spacer,
  Image,
  Button
} from "@chakra-ui/react";

import Card from "components/card/Card.js";
import { useCountdown } from "hooks/useCountdown";
import timerBg from "assets/img/backgrounds/timer_bg.png";
import NFT from "./components/NFT";
import Person from "./components/Person";
//assets
import Nft1 from "assets/img/nfts/nft1.jpg";
import Nft2 from "assets/img/nfts/nft2.jpg";
import Avatar1 from "assets/img/avatars/avatar2.png";
import Avatar2 from "assets/img/avatars/avatar5.png";
import Avatar3 from "assets/img/avatars/avatar6.png";
import { FaFacebook } from 'react-icons/fa';
import { SiTwitter, SiInstagram } from 'react-icons/si';

import React, { useState, useEffect } from "react";

export default function Overview() {

  const brandColor = useColorModeValue("brand.500", "white");
  const boxBg = useColorModeValue("secondaryGray.300", "whiteAlpha.100");

  return (
    <>
      <Flex
        direction="column"
        align="left"
        maxW={{ xl: "1200px" }}
        paddingY={100}
        m="0 auto">
        <Box pt={{ base: "130px", md: "80px", xl: "80px" }} maxW='3xl' h="350px">
          <Text fontSize={42} fontWeight={500}>LOREM IPSUS</Text>
          <Text fontSize={42} fontWeight={500}>LOREM IPSUS</Text>
          <Text fontSize={42} fontWeight={500}>LOREM</Text>
        </Box>
        <Flex
          direction="column"
          justifyContent="space-between"
          maxW={{ xl: "50px" }}
          position='absolute'
          my="50px"
          ml="82%">
          <Box w='100%' p={4}>
            <a href="https://google.com" target="_blank" rel="noreferrer">
              <Icon w={9} h={9} as={FaFacebook} />
            </a>
          </Box>  
          <Box w='100%' p={4}>
            <a href="https://google.com" target="_blank" rel="noreferrer">
            <Icon w={9} h={9} as={SiTwitter} />
            </a>
          </Box>  
          <Box w='100%' p={4}>
            <a href="https://google.com" target="_blank" rel="noreferrer">
              <Icon w={9} h={9} as={SiInstagram} />
            </a>
          </Box>   
        </Flex>
      </Flex>
      <Divider />
      <Flex
        direction="column"
        align="center"
        py={50}
        maxW={{ xl: "1000px" }}
        m="0 auto">
        <Text fontSize={54} fontWeight={500}>WHAT IS MAITRIX?</Text>
        <Text fontSize={24}>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, Lorem ipsum dolor sit amet, consectetuer adipiscing elit, Lorem ipsum dolor sit amet, consectetuer adipiscing elit, Lorem ipsum dolor sit amet, consectetuer adipiscing elit, Lorem ipsum dolor sit amet, consectetuer adipiscing elit, Lorem ipsum dolor sit amet, consectetuer adipiscing elit, Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
        </Text>
      </Flex>
      <Divider />
      <Flex
        direction="column"
        align="left"
        py={50}
        maxW={{ xl: "1200px" }}
        m="0 auto">
        <Text fontSize={32} fontWeight={500}>NFT - CATALOGO X</Text>
        <SimpleGrid py={10} columns={{ base: 1, md: 3 }} gap='20px'>
          <NFT
            name='Abstract Colors'
            description='By Esthera Jackson'
            image={Nft1}
            download={'#'}
            currentbid='0.91 MAI'
            showbids={true}
          />
          <NFT
            name='Abstract Colors'
            description='By Esthera Jackson'
            image={Nft1}
            download={'#'}
            currentbid='0.99 MAI'
            showbids={true}
          />
          <NFT
            name='Abstract Colors'
            description='By Esthera Jackson'
            image={Nft1}
            download={'#'}
            currentbid='0.96 MAI'
            showbids={true}
          />
        </SimpleGrid>
      </Flex>
      <Flex
        direction="column"
        align="left"
        maxW={{ xl: "1200px" }}
        m="0 auto">
        <Text fontSize={32} fontWeight={500}>NFT - CATALOGO Y</Text>
        <SimpleGrid py={10} columns={{ base: 1, md: 3 }} gap='20px'>
          <NFT
            name='Abstract Blue'
            description='By Esthera Jackson'
            image={Nft2}
            download={'#'}
            currentbid='0.91 MAI'
            showbids={true}
          />
          <NFT
            name='Abstract Blue'
            description='By Esthera Jackson'
            image={Nft2}
            download={'#'}
            currentbid='0.99 MAI'
            showbids={true}
          />
          <NFT
            name='Abstract Blue'
            description='By Esthera Jackson'
            image={Nft2}
            download={'#'}
            currentbid='0.96 MAI'
            showbids={true}
          />
        </SimpleGrid>
      </Flex>
      <Flex
        direction="column"
        align="left"
        maxW={{ xl: "1200px" }}
        m="0 auto">
        <Text fontSize={32} fontWeight={500}>Team</Text>
        <SimpleGrid py={10} columns={{ base: 1, md: 3 }} gap='20px'>
          <Person
            name='Ahmed Castro'
            title='Senior blockchain developer'
            image={Avatar1}
          />
          <Person
            name='Sergio Peralta'
            title='Full Stack Developer'
            image={Avatar2}
          />
          <Person
            name='Liu Chan'
            title='Mortal Combat Expert'
            image={Avatar3}
          />
        </SimpleGrid>
      </Flex>
      <Flex
        direction="column"
        align="center"
        maxW={{ xl: "1200px" }}
        m="0 auto"
        py={10}>
        <Box mb={{ base: "20px", "2xl": "20px" }} position='relative'>
          <Text fontSize={32}>LOGO</Text>
        </Box>
        <Text fontSize={18}>Lorem ipsum dolor sit</Text>
        <Text fontSize={18}>Lorem ipsum</Text>
      </Flex>
    </>
  );
}
