
var Web3 = require('web3');
//TODO: sent infura
var web3 = new Web3(Web3.givenProvider || 'ws://some.local-or-remote.node:8546');

const contractABI = require('./contracts/token-presale.json')
export const contractAddress = "0xE13dA1245e4De135B89F52aE0800BDe096FdaB90";

const busdContractABI = require('./contracts/busd.json')
const busdContractAddress = "0xCa2bDAa8a57f679F05277c5331bAf6F73E8AE935";

export const tokenPresaleContract = new web3.eth.Contract(
  contractABI,
  contractAddress
);

export const busdContract = new web3.eth.Contract(
  busdContractABI,
  busdContractAddress
);

export const convertFromWei = (value) => web3.utils.fromWei(value);
export const convertToWei = (value) => web3.utils.toWei(value);